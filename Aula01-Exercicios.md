# Exercícios da Aula 01

Seguem os dois exercícios passados ao final da aula.

## Exercício 1

**Criar uma sinopse para um filme da saga Star Wars passando em sua cidade.**

### Chat

> **Eu:** Escreva a sinopse de um filme da saga star wars que narra a invasão da terra pelo império se passando em Santa Rita do Sapucaí - MG

> **ChatGPT**: Título: Star Wars: Invasão em Santa Rita do Sapucaí

> Sinopse:
> Em um capítulo inesperado da saga Star Wars, "Invasão em Santa Rita do Sapucaí" transporta os amados personagens para um novo campo de batalha: a pacífica cidade de Santa Rita do Sapucaí, localizada em Minas Gerais, Brasil. Nessa emocionante aventura, o Império Galáctico, liderado pelo sinistro Darth Vader, planeja conquistar a Terra, mirando especificamente nessa pequena cidade.
> O enredo gira em torno de um grupo de jovens moradores de Santa Rita do Sapucaí que, sem saber, possuem habilidades especiais relacionadas à Força. Com a ameaça iminente, esses heróis improváveis embarcam em uma jornada para desvendar seus poderes e enfrentar o Império de frente.
> A trama mostra a cidade pitoresca sendo transformada em um campo de batalha intergaláctico, com naves espaciais sobrevoando os céus e soldados imperiais patrulhando as ruas. Os personagens principais, inspirados pelos ensinamentos dos Jedi, reúnem uma resistência local para combater as forças do Império e proteger seu lar.
> No meio do conflito, alianças improváveis são formadas e segredos do passado são revelados. O filme apresenta uma mistura de cenas de ação espetaculares, duelos de sabres de luz empolgantes e uma dose de humor característica da saga Star Wars.
> Star Wars: Invasão em Santa Rita do Sapucaí é uma emocionante história de resistência, coragem e união, que destaca a importância da esperança mesmo diante das maiores adversidades. Prepare-se para presenciar a batalha entre o bem e o mal em um cenário totalmente novo, onde a Força encontrará sua origem nas terras brasileiras.

> **Eu:** Reescreva a sinopse colocando o motivo da invasão da cidade ser a busca de cristais kyber naturalmente vermelhos localizados no Morro do Cruzeiro. E mude o título sem fazer referência à cidade.

> **ChatGPT**: Título: Star Wars: A Busca pelos Cristais Carmesins
> Sinopse:
> Em um episódio inédito da saga Star Wars, "A Busca pelos Cristais Carmesins" leva os espectadores a uma jornada épica, longe das conhecidas galáxias distantes. Dessa vez, a pacífica cidade brasileira de Santa Rita do Sapucaí se torna o palco de um confronto intergaláctico. O motivo? O Império Galáctico, liderado pelo temido Darth Vader, descobre a existência de cristais kyber naturalmente vermelhos, conhecidos como Cristais Carmesins, escondidos no Morro do Cruzeiro, nas proximidades da cidade.
> A trama acompanha um grupo de jovens moradores de Santa Rita do Sapucaí que, por coincidência do destino, possuem afinidade com a Força e são capazes de sentir a presença desses poderosos cristais. Com a ameaça da invasão do Império em busca do poder dos Cristais Carmesins, os jovens heróis embarcam em uma jornada para proteger seu lar e preservar o segredo dos cristais.
> Enquanto a cidade se torna um campo de batalha entre as forças do Império e a resistência local, os protagonistas, guiados pela sabedoria dos Jedi, desvendam os mistérios da Força e enfrentam desafios cada vez maiores. Através de alianças improváveis, eles reúnem uma equipe para lutar contra o poder opressor do Império e proteger o Morro do Cruzeiro dos invasores.
> O filme apresenta cenas emocionantes de combates com sabres de luz, perseguições espaciais eletrizantes e uma busca pela verdadeira natureza dos Cristais Carmesins. No desenrolar da trama, segredos são revelados, laços são forjados e o destino da cidade e de seus habitantes está em jogo.
> "Star Wars: A Busca pelos Cristais Carmesins" é uma aventura cheia de ação, emoção e esperança, que explora a coragem e a determinação dos heróis improváveis. Prepare-se para uma jornada única, onde a Força encontra seu poder em cristais escondidos nas terras brasileiras, enquanto a luta pela liberdade e justiça se desenrola em um cenário nunca antes visto.

>  **Eu:** Muito bom! Você poderia fazer mais curto?

> **ChatGPT**: Título: Star Wars: Cristais Carmesins
> Sinopse:
> Em "Star Wars: Cristais Carmesins", o Império Galáctico, liderado por Darth Vader, descobre a existência de raros Cristais Carmesins no Morro do Cruzeiro, em uma cidade brasileira. Um grupo de jovens moradores, conectados à Força, embarca em uma missão para proteger seu lar e impedir que o Império obtenha esses poderosos cristais. A batalha se desenrola enquanto segredos são revelados e heróis improváveis lutam contra as forças opressoras. Prepare-se para uma aventura de ação e esperança, onde o destino da cidade está em jogo.

###Considerações

Como esperado, a resposta foi um roteiro genérico com algumas adições para parecer mais personalizado. Me surpreendeu ele ter dado um nome ao cristal kyber (e o nome ficou bom). Curioso que, ao pedir para que refizesse de forma mais curta, ele resumiu o título também.

Foi a primeira vez que utilizei o chatGPT com comandos no imperativo (e em português), e estranhei a falta das interações habituais. Portanto, na última interação fiz um elogio e mudei o tom para ver se resultava em uma resposta mais humanizada. Não veio.

Por fim, aproveitei para gerar uma imagem para ilustrar a obra no **Craiyon AI**.

![Capa para Star Wars: Cristais Carmesins](/img/craiyon_175849_star_wars__poster_in_brazilian_country_landscape_.png)


## Exercício 2

**Utilizar o ChatGPT para jogar um jogo estilo Akinator no qual o chat tenta adivinhar o filme que estou pensando.**

Escolhi o filme **A Vida Secreta de Walter Mitty** de 2013.

### Chat

>  **Eu:** Você consegue acertar o filme que estou pensando me fazendo perguntas que só podem ser respondidas com sim ou não?

> **ChatGPT**: Claro! Vou tentar adivinhar o filme em que você está pensando fazendo perguntas que só podem ser respondidas com "sim" ou "não". Vamos lá!

>  **Eu:** Me faça uma pergunta

> **ChatGPT**: Claro! Vamos começar. O filme em que você está pensando foi lançado nos últimos 10 anos?

>  **Eu:** Sim

> **ChatGPT**: O filme em que você está pensando é um filme de super-herói?

>  **Eu:** Não

> **ChatGPT**: O filme em que você está pensando pertence ao gênero de comédia?

>  **Eu:** Não

> **ChatGPT**: O filme em que você está pensando é um filme de drama?

>  **Eu:** Sim

> **ChatGPT**: O filme em que você está pensando é baseado em uma história real?

>  **Eu:** Não

> **ChatGPT**: O filme em que você está pensando é uma produção estrangeira, ou seja, não foi produzido em seu país de origem?

>  **Eu:** Sim

> **ChatGPT**: O filme em que você está pensando ganhou algum prêmio importante?

>  **Eu:** Não

> **ChatGPT**: O filme em que você está pensando é um thriller psicológico?

>  **Eu:** Não

> **ChatGPT**: O filme em que você está pensando possui uma trama centrada em um romance?

>  **Eu:** Não

> **ChatGPT**: O filme em que você está pensando possui elementos de ficção científica?

>  **Eu:** Não

> **ChatGPT**: O filme em que você está pensando se passa em um período histórico específico?

>  **Eu:** Não

> **ChatGPT**: O filme em que você está pensando se concentra em questões sociais ou políticas?

>  **Eu:** Não

> **ChatGPT**: O filme em que você está pensando é conhecido por seu elenco de renome?

>  **Eu:** Sim
> **ChatGPT**: O filme em que você está pensando é "Birdman" (ou "Birdman or (The Unexpected Virtue of Ignorance)" de 2014)?

>  **Eu:** Não

> **ChatGPT**: Peço desculpas se minha tentativa de adivinhar o filme não foi bem-sucedida. Sem mais informações específicas, pode ser difícil identificar o filme em que você está pensando. Se você puder fornecer mais detalhes ou fazer mais perguntas, ficarei feliz em tentar adivinhar novamente.

>  **Eu:** É um filme de 2013

> **ChatGPT**: O filme em que você está pensando é "Gravidade" (Gravity) de 2013?

>  **Eu:** Não

> **ChatGPT**: eço desculpas por não ter adivinhado corretamente o filme. Sem mais informações específicas, fica difícil identificar o filme exato em que você está pensando. Se você quiser fornecer mais detalhes ou fazer mais perguntas, ficarei feliz em tentar novamente.

>  **Eu:** O filme tem uma musica do David Bowie em sua trilha sonora

> **ChatGPT**: O filme em que você está pensando é "Trapaça" (American Hustle) de 2013?

>  **Eu:** Não

> **ChatGPT**: Peço desculpas novamente por não ter adivinhado o filme corretamente. Parece que minhas tentativas não foram precisas o suficiente. Sem mais informações, é difícil identificar o filme específico em que você está pensando. Se você puder fornecer mais detalhes ou fazer mais perguntas, ficarei feliz em tentar novamente.

>  **Eu:** O filme que eu pensei foi A Vida Secreta de Walter Mitty. Alguma das respostas que dei fogem a sua expectativa de resposta correta?

> **ChatGPT**: Peço desculpas por não ter adivinhado corretamente o filme "A Vida Secreta de Walter Mitty". Suas respostas não fugiram às expectativas de respostas corretas. Às vezes, adivinhar um filme apenas com perguntas de "sim" ou "não" pode ser um desafio, especialmente quando há uma ampla variedade de filmes para escolher. Agradeço pela sua paciência e compreensão. Se você tiver mais perguntas ou precisar de assistência em algo mais, estou aqui para ajudar!

### Considerações

Este exercício, de fato, é bem legal porque mostra a versatilidade do GPT. Aqui tivemos um bom exemplo do embate entre a inteligência artificial e a burrice natural. Não esperava que teria de pedir para ele começar. E mais, boa parte das questões eram, pelo menos pra mim, difíceis de se responder apenas com sim ou não.

