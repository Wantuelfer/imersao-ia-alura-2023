![Capa com temática futurísta gegrada pelo Craiyon AI](/img/craiyon_221531_futuristic_background_with_letters__numbers_and_tools_and_written__ai_.png)

# Glossário

> **"O princípio da sabedoria é chamar as coisas pelo nome apropriado."** <br><p style="text-align: right;" >Confúcio</p>  


Pequeno glossário com termos, expressões e conceitos importantes para a compreensão de IAs. Salvo quando indicado, os verbetes foram explicados pela **Perplexity AI**.

## A

**Algoritmo**: É uma sequência de passos finitos e bem definidos que resolvem um problema, seja ele matemático, computacional ou cotidiano[^wikipediaalg][^udealg]. É uma ferramenta utilizada para solucionar problemas de forma sistemática e precisa, seguindo uma sequência lógica de instruções[^wikipediaalg][^udealg]. Em termos de programação, um algoritmo é uma sequência de passos lógicos que permitem solucionar um problema[^wikipediaalg]. Os algoritmos podem ser determinísticos, onde o comportamento é linear e cada passo tem apenas uma opção, ou probabilísticos, onde o comportamento é não linear e cada passo tem várias opções[^wikipediaalg]. Os algoritmos são amplamente utilizados em inteligência artificial, aprendizado de máquina, matemática, ciência da computação e em tarefas cotidianas[^wikipediaalg][^udealg].  

**Algoritmos caixa-preta**: são algoritmos cujo funcionamento interno não é conhecido ou não é importante para o usuário. Em outras palavras, o usuário sabe o que o algoritmo faz, mas não sabe como ele faz. Algoritmos caixa-preta são amplamente utilizados em inteligência artificial e aprendizado de máquina, onde o foco está no desempenho do algoritmo em vez de sua implementação. Por exemplo, um modelo de aprendizado de máquina pode ser considerado uma caixa-preta, pois o usuário sabe que ele é capaz de fazer previsões precisas, mas não sabe como ele chega a essas previsões. Algoritmos caixa-preta são úteis porque permitem que os usuários se concentrem nos resultados em vez dos detalhes de implementação, o que pode ser importante em muitas aplicações práticas.[^udealg][^interglotacp][^ldacp]

**Algoritmos de Aprendizado Não Supervisionado**: são algoritmos de aprendizado de máquina que usam dados não rotulados para aprender a encontrar padrões ou estruturas nos dados. Os dados não rotulados são aqueles que não possuem uma resposta conhecida, permitindo que o algoritmo aprenda a partir da estrutura dos dados[^leotronics].

**Algoritmos de Aprendizado por Reforço**: são algoritmos de aprendizado de máquina que usam um sistema de recompensa para aprender a tomar decisões em um ambiente dinâmico. O algoritmo aprende a partir de tentativa e erro, recebendo recompensas ou penalidades com base em suas ações[^leotronics].

**Algoritmos de Aprendizado Supervisionado**: são algoritmos de aprendizado de máquina que usam dados rotulados para aprender a fazer previsões ou classificações. Os dados rotulados são aqueles que já possuem uma resposta conhecida, permitindo que o algoritmo aprenda a partir de exemplos[^leotronics].

**Aprendizado de Máquina**:  é um subcampo da inteligência artificial que se concentra no desenvolvimento de algoritmos e modelos que permitem que as máquinas aprendam a partir de dados. O aprendizado de máquina é usado em muitas aplicações de IA, como reconhecimento de padrões, classificação e previsão[^leotronics][^pucrs].

**Argumento do Quarto Chinês**: É uma teoria proposta pelo filósofo norte-americano John Searle em 1980, que tem como objetivo contrapor os pesquisadores da área da Inteligência Artificial. O argumento consiste em um experimento mental em que uma pessoa que não entende chinês é colocada em um quarto com um conjunto de regras que lhe permitem responder a perguntas em chinês, sem que ela entenda o significado das palavras. Searle argumenta que, mesmo que a pessoa seja capaz de responder às perguntas em chinês, ela não compreende a linguagem, assim como um computador que executa um programa não compreende o significado das informações que processa. O argumento do quarto chinês gerou muitas controvérsias e respostas na época, pois se o computador não pudesse compreender a linguagem, como poderia ser considerado inteligente? [^iaexpertqc][^knoowqc][^quartochines][^fneqc]

## D

**Deep Learning**:  é a parte do aprendizado de máquina que, por meio de algoritmos de alto nível, reproduz a rede neural do cérebro humano. É uma técnica de aprendizado que permite que as máquinas aprendam a partir de grandes quantidades de dados. O deep learning é usado em muitas aplicações de IA, como reconhecimento de fala, reconhecimento de imagem e processamento de linguagem natural[^pucrs].

**Descida do gradiente**: É um algoritmo de otimização utilizado em inteligência artificial e aprendizado de máquina para encontrar o mínimo local de uma função. Existem duas variações importantes da descida do gradiente: a descida do gradiente em lote (batch) e a descida do gradiente estocástica. As principais diferenças entre elas são: 
- Batch Gradient Descent:

    Calcula o gradiente da função de custo em relação aos parâmetros usando todos os dados de treinamento em cada iteração.
    É muito lento em conjuntos de dados muito grandes, pois requer uma previsão para cada instância no conjunto de dados de treinamento.
    É ótimo para funções de custo convexas ou relativamente suaves.

- Stochastic Gradient Descent:

    Calcula o gradiente da função de custo em relação aos parâmetros usando apenas um lote de dados de treinamento em cada iteração.
    É mais rápido em conjuntos de dados muito grandes, pois requer apenas uma previsão para um lote de dados em cada iteração.
    Pode ser menos preciso do que a descida do gradiente em lote, pois a estimativa do gradiente pode ser ruidosa devido à amostragem aleatória.

Ambas as variações da descida do gradiente são amplamente utilizadas em inteligência artificial e aprendizado de máquina, e a escolha entre elas depende do tamanho do conjunto de dados, da complexidade da função de custo e da disponibilidade de recursos computacionais.[^ufprddg][^acervolimadg][^facuredg][^dlpddg][^dtddg]

## H

**Heurística**: É uma técnica utilizada em ciência da computação, inteligência artificial e otimização matemática para resolver um problema mais rapidamente quando os métodos clássicos são muito lentos ou para encontrar uma solução aproximada quando os métodos clássicos não conseguem encontrar uma solução exata[^wikiheu]. A heurística é uma função que classifica alternativas em algoritmos de pesquisa em cada etapa de ramificação com base nas informações disponíveis para decidir qual ramificação seguir[^wikiheu]. Em outras palavras, é uma função aproximada para algo[^matsumuraheu]. A heurística é uma técnica de busca que pode ser utilizada para a obtenção de metas em problemas não algorítmicos que geram "explosões" combinatórias[^matsumuraheu].  


No contexto de inteligência artificial, a heurística é utilizada como técnica de busca para a obtenção de metas em problemas não algorítmicos[^matsumuraheu]. Por exemplo, um sistema inteligente pode utilizar heurísticas para resolver problemas do dia a dia, como encontrar a melhor rota para chegar a um destino[^passeiheu]. A heurística é uma técnica importante para a inteligência artificial, pois permite que os sistemas inteligentes encontrem soluções para problemas complexos de forma mais rápida e eficiente[^uniaheu].

## I

**Inteligência Artificial (IA)**: é uma variedade de soluções e métodos tecnológicos e científicos que ajudam a fazer programas semelhantes à inteligência humana. A IA inclui muitas ferramentas, algoritmos e sistemas, que também são todos componentes da ciência de dados e das redes neurais artificiais[^leotronics][^fia].

## L

**Lógica fuzzy**: É uma extensão da lógica booleana que permite atribuir a cada proposição um grau de verdade diferente de 0 e 1 e compreendido entre eles[^wikifuzzyi]. É uma lógica polivalente que permite lidar com a incerteza e a imprecisão presentes em muitos problemas do mundo real[^wikifuzzye]. A lógica fuzzy é baseada em conjuntos fuzzy, que são conjuntos cujos elementos têm graus de pertinência em vez de pertencerem ou não pertencerem ao conjunto[^wikifuzzye]. A lógica fuzzy é amplamente utilizada em inteligência artificial, aprendizado de máquina, controle de processos e outras áreas onde a incerteza e a imprecisão são comuns[^wikifuzzye]. A lógica fuzzy tem várias aplicações práticas, como a lógica da diagnóstico clínico e a segurança[^wikifuzzyi]. A lógica fuzzy é uma ferramenta poderosa para lidar com a complexidade e a incerteza do mundo real.

## M

**Matriz de confusão**: É uma tabela que permite avaliar o desempenho de um modelo de classificação em problemas de aprendizado de máquina[^wikimdconf][^cenmdc]. A tabela é composta por duas linhas e duas colunas, onde cada linha representa as classes reais e cada coluna representa as classes previstas pelo modelo[^wikimdconf]. Os valores na diagonal principal da tabela representam as previsões corretas, enquanto os valores fora da diagonal principal representam as previsões incorretas[^wikimdconf]. A matriz de confusão é uma ferramenta importante para avaliar a precisão, a sensibilidade, a especificidade e outras métricas de desempenho de um modelo de classificação[^wikimdconf][^cenmdc]. A matriz de confusão é amplamente utilizada em inteligência artificial, aprendizado de máquina e outras áreas onde a classificação é importante[^wikimdconf][^msmdc].  

**Modelos**: São conjuntos de soluções, algoritmos e técnicas que são treinados a partir de informações e imagens prévias e têm relação de linguagem com as máquinas[^meioemensagem][^abapm]. Eles são utilizados para simular a inteligência humana, realizando determinadas atividades de maneira autônoma e aprendendo por si mesmos, graças ao processamento de um grande volume de dados que recebem de seus usuários[^tableum]. Os modelos de linguagem naturais são um exemplo de modelo de inteligência artificial, que consolidam-se como conjunto de soluções, algoritmos e técnicas que, dentro da IA, tenham relação de linguagem com as máquinas[^meioemensagem][^abapm]. Os diferentes modelos de inteligência artificial são classificados de acordo com a sua funcionalidade e capacidade[^uolm]. Eles são utilizados para tornar processos como a tomada de decisões e a produção muito mais rápidos e eficazes, sendo muito úteis na otimização de rotinas diárias[^uolm].

## P

**Parâmetros**: São valores que são ajustados durante o treinamento de um modelo de aprendizado de máquina para melhorar sua precisão e desempenho. Esses parâmetros podem incluir coisas como o tamanho da rede neural, a taxa de aprendizado, o número de épocas de treinamento e o tamanho do lote de treinamento. O ajuste desses parâmetros é uma parte importante do processo de treinamento de um modelo de aprendizado de máquina, pois pode afetar significativamente a precisão e a eficácia do modelo. Alguns dos parâmetros mais comuns em modelos de aprendizado de máquina incluem:

- Taxa de aprendizado: é a taxa na qual o modelo ajusta seus pesos durante o treinamento.

- Número de épocas: é o número de vezes que o modelo passa pelo conjunto de treinamento completo durante o treinamento.

- Tamanho do lote de treinamento: é o número de exemplos de treinamento que o modelo usa para atualizar seus pesos em cada iteração.

- Tamanho da rede neural: é o número de camadas e unidades em cada camada da rede neural.

- Função de ativação: é a função matemática que é aplicada a cada unidade em uma rede neural para determinar sua saída.

O ajuste desses parâmetros pode ser um processo complexo e requer experimentação para determinar os valores ideais para um determinado modelo e conjunto de dados[^leotronics][^cmkls].


**Processamento de Linguagem Natural (PLN)**: é uma subárea da inteligência artificial que se concentra em permitir que as máquinas entendam e processem a linguagem humana. O PLN é usado em muitas aplicações de IA, como chatbots, assistentes virtuais e análise de sentimentos[^pangeanic].  

**Prompt**: É um termo utilizado em inteligência artificial para se referir a uma mensagem ou instrução que é dada a um sistema inteligente para que ele possa realizar uma tarefa específica[^uniaheu]. Em outras palavras, o prompt é uma entrada de dados que é fornecida ao sistema para que ele possa processá-la e fornecer uma saída correspondente[^passeiheu]. O prompt pode ser fornecido em diferentes formatos, como texto, imagem ou som, dependendo do tipo de sistema inteligente e da tarefa que ele deve realizar[^uspprompt]. Por exemplo, um assistente virtual pode receber um prompt de voz para realizar uma pesquisa na internet ou enviar uma mensagem para um contato específico[^matsumuraheu]. O prompt é uma parte importante da interação entre humanos e sistemas inteligentes, pois permite que os usuários forneçam informações e comandos de forma natural e intuitiva[^sofpromp].

## R

**Redes Neurais Artificiais**: são técnicas computacionais que apresentam um modelo matemático inspirado na estrutura neural de organismos inteligentes e que adquirem conhecimento através da experiência. Uma grande rede neural artificial pode ter centenas ou milhares de unidades de processamento; já o cérebro de um mamífero pode ter muitos bilhões de neurônios[^leotronics][^cmkls][^usp].

## T

**Transformers**: Os transformers são modelos de redes neurais que utilizam técnicas matemáticas de atenção ou autoatenção para aprender o contexto e o significado de dados sequenciais, como textos, mesmo em elementos que possuem dados distantes[^nvidia][^zup][^deeplearningbook].

Eles são capazes de detectar as maneiras sutis como até mesmo elementos de dados distantes em uma série influenciam e dependem uns dos outros[^nvidia].

Os transformers são descritos como uma das arquiteturas de deep learning mais novas e potentes já inventadas até hoje[^nvidia].

Eles estão impulsionando uma onda de avanços em machine learning, o que alguns chamam de AI de transformers[^nvidia].

Os modelos transformers atuam basicamente como grandes blocos que processam e transformam dados em atividades de encoder (codificação) e decoder (decodificação)[^zup].

Eles conseguem codificar dados sequenciais, como textos, de forma posicional, marcando os elementos e fornecendo um contexto[^zup].

Os transformers são capazes de lidar com dependências de longo alcance com facilidade, o que os torna ideais para resolver tarefas sequence-to-sequence[^deeplearningbook].

Desde 2018, têm surgido insights de pesquisas sobre os modelos transformers, que são redes neurais que fornecem informações a partir de um contexto predeterminado[^meioemensagem].

## U

**Unidades de Processamento**: São componentes de uma rede neural artificial que processam informações ao responder a entradas externas, como neurônios no cérebro humano. As unidades de processamento são interconectadas e organizadas em camadas, e cada unidade recebe entradas de outras unidades ou de entradas externas. As unidades de processamento são responsáveis por realizar cálculos matemáticos complexos em grandes conjuntos de dados, permitindo que as redes neurais artificiais aprendam a partir de exemplos e melhorem sua precisão e desempenho ao longo do tempo. O número de unidades de processamento em uma rede neural pode variar de algumas centenas a milhões, dependendo da complexidade da tarefa que a rede neural está tentando realizar[^leotronics][^sasup].

## V

**Viés**: Viés na inteligência artificial é a tendência de um algoritmo ou sistema de aprendizado de máquina de produzir resultados imprecisos ou discriminatórios devido a preconceitos ou desigualdades presentes nos dados de treinamento. Esses vieses podem ser introduzidos por uma variedade de fatores, incluindo a seleção de dados de treinamento, a escolha de recursos ou variáveis de entrada, e a própria arquitetura do modelo de aprendizado de máquina. O viés pode levar a resultados injustos ou discriminatórios em áreas como seleção de candidatos, avaliação de crédito, justiça criminal e outras aplicações de tomada de decisão automatizada. Para evitar vieses na inteligência artificial, é importante garantir que os dados de treinamento sejam representativos e diversificados, que os algoritmos sejam projetados para minimizar o viés, e que haja supervisão humana para monitorar e corrigir quaisquer resultados injustos ou discriminatórios[^neuralmindvies][^tecmasteriesvies].

**Visão computacional**: É uma área da ciência da computação e da inteligência artificial que busca analisar, interpretar e extrair informações relevantes de imagens e vídeos[^dsacdevc][^awsvc]. A visão computacional utiliza técnicas de processamento de imagens, aprendizado de máquina e outras técnicas de inteligência artificial para permitir que os sistemas "vejam" e interpretem o mundo visual[^cubovc]. A visão computacional é amplamente utilizada em aplicações como reconhecimento facial, análise de imagens médicas, detecção de objetos em tempo real, entre outras[^cubovc][^passeivc]. A visão computacional é uma área em constante evolução, com novas técnicas e algoritmos sendo desenvolvidos para lidar com problemas cada vez mais complexos[^santovc]. A visão computacional é uma ferramenta poderosa para a análise de imagens e vídeos em uma ampla variedade de aplicações, desde a medicina até a segurança e o entretenimento.


## Referências

[^abapm]: [ABAP](https://www.abap.com.br/inteligencia-artificial-o-que-sao-modelos-de-linguagem-e-para-que-servem/)
[^acervolimadg]:[Acervo Lima](https://acervolima.com/diferenca-entre-a-descida-do-gradiente-em-lote-e-a-descida-do-gradiente-estocastico/)
[^awsvc]: [AWS](https://aws.amazon.com/pt/what-is/computer-vision/)
[^cmkls]: [cm-kls-content](https://cm-kls-content.s3.amazonaws.com/201802/INTERATIVAS_2_0/INTELIGENCIA_ARTIFICIAL/U1/LIVRO_UNICO.pdf)
[^cenmdc]: [PRATES, Wlademir Ribeiro](https://cienciaenegocios.com/o-que-e-a-matriz-de-confusao/)
[^cubovc]: [Cubo UP](https://cuboup.com/conteudo/inteligencia-artificial/)
[^dsacdevc]: [Data Science Academy](https://blog.dsacademy.com.br/o-que-e-visao_computacional/)
[^dlpddg]:[Deep Learning Book](https://www.deeplearningbook.com.br/aprendizado-com-a-descida-do-gradiente/)
[^deeplearningbook]: [Deep Learning Book](https://www.deeplearningbook.com.br/transformadores-o-estado-da-arte-em-processamento-de-linguagem-natural/)
[^dtddg]:[Didática Tech](https://www.youtube.com/watch?v=snXZg11IJ-8)
[^facuredg]:[FACURE, Matheus](https://matheusfacure.github.io/2017/02/20/MQO-Gradiente-Descendente/)
[^fia]: [FIA](https://fia.com.br/blog/inteligencia-artificial/amp/)
[^fneqc]: [Filosofia na Escola](https://filosofianaescola.com/metafisica/quarto-chines/)
[^iaexpertqc]: [IA Expert](https://iaexpert.academy/2017/02/14/argumento-do-quarto-chines/)
[^interglotacp]: [Interglot](https://interglot.com/es/en/algoritmo)
[^knoowqc]: [Knoow](https://knoow.net/ciencinformtelec/informatica/teoria-do-quarto-chines/)
[^leotronics]: [Leotronics](https://leotronics.eu/pt/nosso-blog/inteligencia-artificial-e-redes-neurais-artificiais)
[^ldacp]: [Let's Data](https://youtube.com/watch?v=vS6bQy45rdE)
[^matsumuraheu]: [MATSUMURA, Ricardo](https://ricardomatsumura.medium.com/algoritmos-de-busca-para-intelig%C3%AAncia-artificial-7cb81172396c)
[^meioemensagem]: [meio&mensagem](https://www.meioemensagem.com.br/proxxima/o-que-sao-modelos-de-linguagem)
[^msmdc]: [Microsoft](https://learn.microsoft.com/pt-br/dynamics365/finance/finance-insights/confusion-matrix)
[^neuralmindvies]: [Neural Mind](https://neuralmind.ai/2021/04/22/como-evitar-vieses-na-inteligencia-artificial/)
[^nvidia]: [NVIDIA](https://blog.nvidia.com.br/2022/04/19/o-que-e-um-modelo-transformer/)
[^pangeanic]: [Pangeanic](https://blog.pangeanic.com/pt-br/redes-neurais-intelig%C3%AAncia-artificial-aplicada-ao-processamento-de-linguagem-natural)
[^passeiheu]: [Passei Direto](https://www.passeidireto.com/arquivo/97729646/atividade-1-gra-1563-inteligencia-artificial-e-aprendizado-de-maquina-gr-1694-21)
[^passeivc]: [Passei Direto](https://www.passeidireto.com/arquivo/102708628/visao-computacional)
[^pucrs]: [PUC RS](https://online.pucrs.br/blog/public/inteligencia-artificial-conceito)
[^santovc]: [Santo Digital](https://santodigital.com.br/o-que-e-visao-computacional-e-para-que-serve/)
[^sasup]: [SAS](https://www.sas.com/pt_br/insights/analytics/what-is-artificial-intelligence.html)
[^sofpromp]: [Stackoverflow](https://pt.stackoverflow.com/questions/357809/o-que-%C3%A9-uma-heur%C3%ADstica)
[^tableum]: [Tableau](https://www.tableau.com/pt-br/learn/articles/ai)
[^tecmasteriesvies]: [TecMasters](https://tecmasters.com.br/inteligencia-artificial-com-vies-afeta-empresas/)
[^udealg]: [UDE] (https://ude.edu.uy/que-son-algoritmos/amp/)
[^uolm]: [Brasil Escola](https://brasilescola.uol.com.br/informatica/inteligencia-artificial.htm)
[^uniaheu]: [UNIASSELVI](https://www.uniasselvi.com.br/extranet/layout/request/trilha/materiais/gabarito/gabarito.php?codigo=22869)
[^ufprddg]:[UFPR](https://cursos.leg.ufpr.br/ML4all/apoio/Gradiente.html)
[^usp]: [USP](https://sites.icmc.usp.br/andre/research/neural/)
[^uspprompt]: [USP](https://www2.eca.usp.br/smct/ojs/index.php/smct/article/viewFile/64/63)
[^wikipediaalg]: [Wikipedia](https://es.wikipedia.org/wiki/Algoritmo)
[^wikifuzzye]: [Wikipedia](https://en.wikipedia.org/wiki/Fuzzy_logic)
[^wikifuzzyi]: [Wikipedia](https://it.wikipedia.org/wiki/Logica_fuzzy)
[^wikimdconf]: [wikipedia](https://pt.wikipedia.org/wiki/Matriz_de_confus%C3%A3o)
[^wikiheu]: [Wikipedia](https://pt.wikipedia.org/wiki/Heur%C3%ADstica_(computa%C3%A7%C3%A3o))
[^quartochines]: [Wikipedia](https://pt.wikipedia.org/wiki/Quarto_chin%C3%AAs)
[^zup]: [Zup](https://www.zup.com.br/blog/modelo-transformer)
